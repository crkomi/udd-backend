package com.crkomi.uddbackend.entities;

import com.crkomi.uddbackend.lucine.model.IndexUnit;

import javax.persistence.*;

@Entity
public class EBookEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String title;
    private String author;
    private String keywords;
    private Integer publicationYear;
    private String fileName;
    private String mime;
    private String highlight;
    private String text;

    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    private LanguageEntity languageEntity;

    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    private CategoryEntity categoryEntity;

    //@ManyToOne(fetch = FetchType.EAGER, optional = true)
    //private UserEntity userEntity;

    public EBookEntity(String title, String author, String keywords, Integer publicationYear, String fileName, String mime) {
        this.title = title;
        this.author = author;
        this.keywords = keywords;
        this.publicationYear = publicationYear;
        this.fileName = fileName;
        this.mime = mime;
    }

    public EBookEntity(){
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Integer getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(Integer publicationYear) {
        this.publicationYear = publicationYear;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public LanguageEntity getLanguageEntity() {
        return languageEntity;
    }

    public void setLanguageEntity(LanguageEntity languageEntity) {
        this.languageEntity = languageEntity;
    }

    public CategoryEntity getCategoryEntity() {
        return categoryEntity;
    }

    public void setCategoryEntity(CategoryEntity categoryEntity) {
        this.categoryEntity = categoryEntity;
    }

    public String getHighlight() {
        return highlight;
    }

    public void setHighlight(String highlight) {
        this.highlight = highlight;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
