package com.crkomi.uddbackend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class CategoryEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "categoryEntity")
    private List<EBookEntity> eBookEntityList = new ArrayList<>();

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "categoryEntity")
    private List<UserEntity> userEntities = new ArrayList<>();

    public CategoryEntity(String name) {
        this.name = name;
    }

    public CategoryEntity(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EBookEntity> geteBookEntityList() {
        return eBookEntityList;
    }

    public void seteBookEntityList(List<EBookEntity> eBookEntityList) {
        this.eBookEntityList = eBookEntityList;
    }

    public List<UserEntity> getUserEntities() {
        return userEntities;
    }

    public void setUserEntities(List<UserEntity> userEntities) {
        this.userEntities = userEntities;
    }
}
