package com.crkomi.uddbackend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class LanguageEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "languageEntity")
    private List<EBookEntity> eBookEntityList = new ArrayList<>();


    public LanguageEntity(String name) {
        this.name = name;
    }

    public LanguageEntity(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EBookEntity> geteBookEntityList() {
        return eBookEntityList;
    }

    public void seteBookEntityList(List<EBookEntity> eBookEntityList) {
        this.eBookEntityList = eBookEntityList;
    }
}
