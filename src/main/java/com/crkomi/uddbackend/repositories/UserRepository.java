package com.crkomi.uddbackend.repositories;

import com.crkomi.uddbackend.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    @Query("select u from UserEntity u where u.username = :username and u.password = :password")
    public List<UserEntity> findByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

    @Query("select u from UserEntity u where u.username = :username")
    public List<UserEntity> findByUsername(@Param("username") String username);

}
