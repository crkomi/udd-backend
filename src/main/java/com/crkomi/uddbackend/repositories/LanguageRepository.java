package com.crkomi.uddbackend.repositories;

import com.crkomi.uddbackend.entities.LanguageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LanguageRepository extends JpaRepository<LanguageEntity, Long> {
}
