package com.crkomi.uddbackend.repositories;

import com.crkomi.uddbackend.entities.EBookEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EBookRepository extends JpaRepository<EBookEntity, Long> {
}
