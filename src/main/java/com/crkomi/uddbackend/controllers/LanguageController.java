package com.crkomi.uddbackend.controllers;

import com.crkomi.uddbackend.entities.CategoryEntity;
import com.crkomi.uddbackend.entities.LanguageEntity;
import com.crkomi.uddbackend.services.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/languages")
public class LanguageController {

    @Autowired
    private LanguageService languageService;

    @CrossOrigin(origins="*")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<LanguageEntity>> getAll(){
        return new ResponseEntity<List<LanguageEntity>>(languageService.findAll(), HttpStatus.OK);
    }

}
