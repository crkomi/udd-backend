package com.crkomi.uddbackend.controllers;

import com.crkomi.uddbackend.entities.CategoryEntity;
import com.crkomi.uddbackend.entities.EBookEntity;
import com.crkomi.uddbackend.entities.UserEntity;
import com.crkomi.uddbackend.lucine.indexing.Indexer;
import com.crkomi.uddbackend.lucine.model.IndexUnit;
import com.crkomi.uddbackend.services.EBookService;
import com.crkomi.uddbackend.services.PdfFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.net.URL;

@RestController
@RequestMapping("/api/ebooks")
public class EBookController {

    @Autowired
    private EBookService eBookService;

    @Autowired
    private PdfFileService pdfFileService;

    @Autowired
    private Indexer indexer;

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<EBookEntity> getOne(@PathVariable("id") Long id){
        return new ResponseEntity<EBookEntity>(eBookService.findOne(id), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<EBookEntity> update(@PathVariable("id") Long id, @RequestBody EBookEntity eBookEntity){
        EBookEntity existingEBookEntity = eBookService.findOne(id);
        if(existingEBookEntity == null){
            return new ResponseEntity<EBookEntity>(HttpStatus.NOT_FOUND);
        }
        if(eBookEntity == null){
            return new ResponseEntity<EBookEntity>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<EBookEntity>(eBookService.save(eBookEntity), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<EBookEntity> create(@RequestBody EBookEntity eBookEntity){
        if(eBookEntity == null){
            return new ResponseEntity<EBookEntity>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<EBookEntity>(eBookService.save(eBookEntity), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<EBookEntity> delete(@PathVariable("id") Long id){
        EBookEntity eBookEntity = eBookService.findOne(id);
        if(eBookEntity == null){
            return new ResponseEntity<EBookEntity>(HttpStatus.NOT_FOUND);
        }
        eBookService.delete(id);
        return new ResponseEntity<EBookEntity>(eBookEntity, HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ResponseEntity<EBookEntity> uploadFile(@RequestBody MultipartFile file) {
        if (file == null) {
            return new ResponseEntity<EBookEntity>(HttpStatus.BAD_REQUEST);
        }
        System.out.println("upload");
        System.out.println(file);
        return new ResponseEntity<EBookEntity>(pdfFileService.saveUploadedFile(file), HttpStatus.OK);
    }

}
