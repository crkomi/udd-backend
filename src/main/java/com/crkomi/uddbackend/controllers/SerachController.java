package com.crkomi.uddbackend.controllers;

import com.crkomi.uddbackend.entities.EBookEntity;
import com.crkomi.uddbackend.lucine.model.*;
import com.crkomi.uddbackend.lucine.search.QueryBuilder;
import com.crkomi.uddbackend.lucine.search.ResultRetriever;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class SerachController {

    @Autowired
    private ResultRetriever resultRetriever;

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/search/term", method = RequestMethod.POST)
    public ResponseEntity<List<EBookEntity>> searchTermQuery(@RequestBody SimpleQuery simpleQuery) throws Exception {
        org.elasticsearch.index.query.QueryBuilder query= QueryBuilder.buildQuery(SearchType.regular, simpleQuery.getField(), simpleQuery.getValue());
        List<RequiredHighlight> rh = new ArrayList<RequiredHighlight>();
        rh.add(new RequiredHighlight(simpleQuery.getField(), simpleQuery.getValue()));
        return new ResponseEntity<List<EBookEntity>>(resultRetriever.getResults(query, rh), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/search/fuzzy", method = RequestMethod.POST)
    public ResponseEntity<List<EBookEntity>> searchFuzzy(@RequestBody SimpleQuery simpleQuery) throws Exception {
        org.elasticsearch.index.query.QueryBuilder query= QueryBuilder.buildQuery(SearchType.fuzzy, simpleQuery.getField(), simpleQuery.getValue());
        List<RequiredHighlight> rh = new ArrayList<RequiredHighlight>();
        rh.add(new RequiredHighlight(simpleQuery.getField(), simpleQuery.getValue()));
        return new ResponseEntity<List<EBookEntity>>(resultRetriever.getResults(query, rh), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/search/prefix", method = RequestMethod.POST)
    public ResponseEntity<List<EBookEntity>> searchPrefix(@RequestBody SimpleQuery simpleQuery) throws Exception {
        org.elasticsearch.index.query.QueryBuilder query= QueryBuilder.buildQuery(SearchType.prefix, simpleQuery.getField(), simpleQuery.getValue());
        List<RequiredHighlight> rh = new ArrayList<RequiredHighlight>();
        rh.add(new RequiredHighlight(simpleQuery.getField(), simpleQuery.getValue()));
        return new ResponseEntity<List<EBookEntity>>(resultRetriever.getResults(query, rh), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/search/range", method = RequestMethod.POST)
    public ResponseEntity<List<EBookEntity>> searchRange(@RequestBody SimpleQuery simpleQuery) throws Exception {
        org.elasticsearch.index.query.QueryBuilder query= QueryBuilder.buildQuery(SearchType.range, simpleQuery.getField(), simpleQuery.getValue());
        List<RequiredHighlight> rh = new ArrayList<RequiredHighlight>();
        rh.add(new RequiredHighlight(simpleQuery.getField(), simpleQuery.getValue()));
        return new ResponseEntity<List<EBookEntity>>(resultRetriever.getResults(query, rh), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/search/phrase", method = RequestMethod.POST)
    public ResponseEntity<List<EBookEntity>> searchPhrase(@RequestBody SimpleQuery simpleQuery) throws Exception {
        org.elasticsearch.index.query.QueryBuilder query= QueryBuilder.buildQuery(SearchType.phrase, simpleQuery.getField(), simpleQuery.getValue());
        List<RequiredHighlight> rh = new ArrayList<RequiredHighlight>();
        rh.add(new RequiredHighlight(simpleQuery.getField(), simpleQuery.getValue()));
        return new ResponseEntity<List<EBookEntity>>(resultRetriever.getResults(query, rh), HttpStatus.OK);
    }



    @CrossOrigin(origins="*")
    @RequestMapping(value = "/search/boolean", method = RequestMethod.POST)
    public ResponseEntity<List<EBookEntity>> searchBoolean(@RequestBody AdvancedQuery advancedQuery) throws Exception {
        org.elasticsearch.index.query.QueryBuilder query1 = QueryBuilder.buildQuery(SearchType.regular, advancedQuery.getField1(), advancedQuery.getValue1());
        org.elasticsearch.index.query.QueryBuilder query2 = QueryBuilder.buildQuery(SearchType.regular, advancedQuery.getField2(), advancedQuery.getValue2());

        BoolQueryBuilder builder = QueryBuilders.boolQuery();
        if(advancedQuery.getOperation().equalsIgnoreCase("AND")){
            builder.must(query1);
            builder.must(query2);
        }else if(advancedQuery.getOperation().equalsIgnoreCase("OR")){
            builder.should(query1);
            builder.should(query2);
        }else if(advancedQuery.getOperation().equalsIgnoreCase("NOT")){
            builder.must(query1);
            builder.mustNot(query2);
        }

        List<RequiredHighlight> rh = new ArrayList<RequiredHighlight>();
        rh.add(new RequiredHighlight(advancedQuery.getField1(), advancedQuery.getValue1()));
        rh.add(new RequiredHighlight(advancedQuery.getField2(), advancedQuery.getValue2()));
        return new ResponseEntity<List<EBookEntity>>(resultRetriever.getResults(builder, rh), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/search/queryParser", method = RequestMethod.POST)
    public ResponseEntity<List<EBookEntity>> search(@RequestBody SimpleQuery simpleQuery) throws Exception {
        org.elasticsearch.index.query.QueryBuilder query= QueryBuilders.queryStringQuery(simpleQuery.getValue());
        List<RequiredHighlight> rh = new ArrayList<RequiredHighlight>();
        return new ResponseEntity<List<EBookEntity>>(resultRetriever.getResults(query, rh), HttpStatus.OK);
    }
}
