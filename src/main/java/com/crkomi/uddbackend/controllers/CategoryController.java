package com.crkomi.uddbackend.controllers;

import com.crkomi.uddbackend.entities.CategoryEntity;
import com.crkomi.uddbackend.entities.EBookEntity;
import com.crkomi.uddbackend.entities.UserEntity;
import com.crkomi.uddbackend.services.CategoryService;
import com.crkomi.uddbackend.services.EBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private EBookService eBookService;

    @CrossOrigin(origins="*")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<CategoryEntity>> getAll(){
        return new ResponseEntity<List<CategoryEntity>>(categoryService.findAll(), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<CategoryEntity> getOne(@PathVariable("id") Long id){
        return new ResponseEntity<CategoryEntity>(categoryService.findOne(id), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/{category_id}/ebooks", method = RequestMethod.GET)
    public ResponseEntity<List<EBookEntity>> getAll(@PathVariable("category_id") Long categoryId){
        CategoryEntity categoryEntity = categoryService.findOne(categoryId);
        if(categoryEntity == null){
            return new ResponseEntity<List<EBookEntity>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<EBookEntity>>(categoryEntity.geteBookEntityList(), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/{category_id}/ebooks/{ebook_id}", method = RequestMethod.GET)
    public ResponseEntity<EBookEntity> getOne(@PathVariable("category_id") Long categoryId, @PathVariable("ebook_id") Long ebookId){
        CategoryEntity categoryEntity = categoryService.findOne(categoryId);
        EBookEntity eBookEntity = eBookService.findOne(ebookId);
        if(categoryEntity == null || eBookEntity == null || !eBookEntity.getCategoryEntity().getId().equals(categoryEntity.getId())){
            return new ResponseEntity<EBookEntity>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<EBookEntity>(eBookEntity, HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<CategoryEntity> update(@PathVariable("id") Long id, @RequestBody CategoryEntity categoryEntity){
        CategoryEntity existingCategoryEntity = categoryService.findOne(id);
        if(existingCategoryEntity == null){
            return new ResponseEntity<CategoryEntity>(HttpStatus.NOT_FOUND);
        }
        if(categoryEntity == null){
            return new ResponseEntity<CategoryEntity>(HttpStatus.BAD_REQUEST);
        }
        if(categoryService.checkName(categoryEntity)){
            return new ResponseEntity<CategoryEntity>(categoryService.save(categoryEntity), HttpStatus.OK);
        }else{
            return new ResponseEntity<CategoryEntity>(HttpStatus.BAD_REQUEST);
        }

    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<CategoryEntity> create(@RequestBody CategoryEntity categoryEntity){
        System.out.println("Pogodjen post category");
        if(categoryEntity == null){
            return new ResponseEntity<CategoryEntity>(HttpStatus.BAD_REQUEST);
        }
        if(categoryService.checkName(categoryEntity)){
            return new ResponseEntity<CategoryEntity>(categoryService.save(categoryEntity), HttpStatus.OK);
        }else{
            return new ResponseEntity<CategoryEntity>(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<CategoryEntity> delete(@PathVariable("id") Long id){
        CategoryEntity categoryEntity= categoryService.findOne(id);
        if(categoryEntity == null){
            return new ResponseEntity<CategoryEntity>(HttpStatus.NOT_FOUND);
        }
        categoryService.delete(id);
        return new ResponseEntity<CategoryEntity>(categoryEntity, HttpStatus.OK);
    }

}
