package com.crkomi.uddbackend.controllers;

import com.crkomi.uddbackend.entities.CategoryEntity;
import com.crkomi.uddbackend.entities.EBookEntity;
import com.crkomi.uddbackend.entities.LanguageEntity;
import com.crkomi.uddbackend.entities.UserEntity;
import com.crkomi.uddbackend.services.CategoryService;
import com.crkomi.uddbackend.services.EBookService;
import com.crkomi.uddbackend.services.LanguageService;
import com.crkomi.uddbackend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Controller;

import java.io.File;
import java.net.URL;

@Controller
public class InitController implements CommandLineRunner {

    private static final String groupsPath ="./src/main/resources/properties/groups.yml";
    private static final String usersPath ="./src/main/resources/static";
    public static String abspluteResourceFile;

    @Autowired
    private LanguageService languageService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private UserService userService;

    @Autowired
    private EBookService eBookService;

    @Override
    public void run(String... args) throws Exception {
        File file = new File(usersPath);
        System.out.println("Absolute resource file is: " + file.getAbsolutePath());
        abspluteResourceFile = file.getAbsolutePath();
        LanguageEntity languageEntity1 = new LanguageEntity();
        languageEntity1.setName("SerbianCir");

        LanguageEntity languageEntity2 = new LanguageEntity();
        languageEntity2.setName("SerbianLat");

        languageEntity1 = languageService.save(languageEntity1);
        languageEntity2 = languageService.save(languageEntity2);

        CategoryEntity categoryEntity1 = new CategoryEntity();
        categoryEntity1.setName("Novel");

        CategoryEntity categoryEntity2 = new CategoryEntity();
        categoryEntity2.setName("Movie");

        CategoryEntity categoryEntity3 = new CategoryEntity();
        categoryEntity3.setName("Thriller");

        CategoryEntity categoryEntity4 = new CategoryEntity();
        categoryEntity4.setName("Horror");

        CategoryEntity categoryEntityAll = new CategoryEntity();
        categoryEntityAll.setName("All");

        categoryEntity1 = categoryService.save(categoryEntity1);
        categoryEntity2 = categoryService.save(categoryEntity2);
        categoryEntity3 = categoryService.save(categoryEntity3);
        categoryEntity4 = categoryService.save(categoryEntity4);
        categoryEntityAll = categoryService.save(categoryEntityAll);

        UserEntity userEntity1 = new UserEntity();
        userEntity1.setFirstname("Mirko");
        userEntity1.setLastname("Odalovic");
        userEntity1.setPassword("crkomi");
        userEntity1.setType(UserService.ADMIN);
        userEntity1.setUsername("crkomi");
        userEntity1.setCategoryEntity(categoryEntityAll);

        UserEntity userEntity2 = new UserEntity();
        userEntity2.setFirstname("Marko");
        userEntity2.setLastname("Grbic");
        userEntity2.setPassword("marko");
        userEntity2.setType(UserService.SUBSCRIBER);
        userEntity2.setUsername("marko");
        userEntity2.setCategoryEntity(categoryEntityAll);

        UserEntity userEntity3 = new UserEntity();
        userEntity3.setFirstname("Zoran");
        userEntity3.setLastname("Milicic");
        userEntity3.setPassword("zoran");
        userEntity3.setType(UserService.SUBSCRIBER);
        userEntity3.setUsername("zoran");
        userEntity3.setCategoryEntity(categoryEntity1);

        userEntity1 = userService.save(userEntity1);
        userEntity2 = userService.save(userEntity2);
        userEntity3 = userService.save(userEntity3);

        EBookEntity eBookEntity1 = new EBookEntity();
        eBookEntity1.setAuthor("Mirko1");
        eBookEntity1.setCategoryEntity(categoryEntity1);
        eBookEntity1.setFileName("E:\\Master\\Upravljanje Digitalnim Dokumentima\\Project\\Project\\udd-backend\\src\\main\\resources\\static\\eBookRepository.pdf");
        eBookEntity1.setKeywords("Keywords1");
        eBookEntity1.setLanguageEntity(languageEntity1);
        eBookEntity1.setMime("Mime1");
        eBookEntity1.setPublicationYear(2015);
        eBookEntity1.setTitle("Title1");

        EBookEntity eBookEntity2 = new EBookEntity();
        eBookEntity2.setAuthor("Mirko2");
        eBookEntity2.setCategoryEntity(categoryEntity2);
        eBookEntity2.setFileName("E:\\Master\\Upravljanje Digitalnim Dokumentima\\Project\\Project\\udd-backend\\src\\main\\resources\\static\\Koridor 10 do kraja 2010.pdf");
        eBookEntity2.setKeywords("Keywords2");
        eBookEntity2.setLanguageEntity(languageEntity1);
        eBookEntity2.setMime("Mime2");
        eBookEntity2.setPublicationYear(2015);
        eBookEntity2.setTitle("Title2");

        EBookEntity eBookEntity3 = new EBookEntity();
        eBookEntity3.setAuthor("Mirko3");
        eBookEntity3.setCategoryEntity(categoryEntity3);
        eBookEntity3.setFileName("E:\\Master\\Upravljanje Digitalnim Dokumentima\\Project\\Project\\udd-backend\\src\\main\\resources\\static\\Otvorene dve deonicekoridora 10.pdf");
        eBookEntity3.setKeywords("Keywords3");
        eBookEntity3.setLanguageEntity(languageEntity1);
        eBookEntity3.setMime("Mime3");
        eBookEntity3.setPublicationYear(2015);
        eBookEntity3.setTitle("Title3");

        EBookEntity eBookEntity4 = new EBookEntity();
        eBookEntity4.setAuthor("Mirko4");
        eBookEntity4.setCategoryEntity(categoryEntity4);
        eBookEntity4.setFileName("E:\\Master\\Upravljanje Digitalnim Dokumentima\\Project\\Project\\udd-backend\\src\\main\\resources\\static\\Pandemija gripa.pdf");
        eBookEntity4.setKeywords("Keywords4");
        eBookEntity4.setLanguageEntity(languageEntity2);
        eBookEntity4.setMime("Mime4");
        eBookEntity4.setPublicationYear(2015);
        eBookEntity4.setTitle("Title4");

        EBookEntity eBookEntity5 = new EBookEntity();
        eBookEntity5.setAuthor("Mirko5");
        eBookEntity5.setCategoryEntity(categoryEntity1);
        eBookEntity5.setFileName("E:\\Master\\Upravljanje Digitalnim Dokumentima\\Project\\Project\\udd-backend\\src\\main\\resources\\static\\Prve vakcine danas u Srbiji.pdf");
        eBookEntity5.setKeywords("Keywords5");
        eBookEntity5.setLanguageEntity(languageEntity2);
        eBookEntity5.setMime("Mime5");
        eBookEntity5.setPublicationYear(2015);
        eBookEntity5.setTitle("Title5");

        eBookEntity1 = eBookService.save(eBookEntity1);
        eBookEntity2 = eBookService.save(eBookEntity2);
        eBookEntity3 = eBookService.save(eBookEntity3);
        eBookEntity4 = eBookService.save(eBookEntity4);
        eBookEntity5 = eBookService.save(eBookEntity5);
    }

}
