package com.crkomi.uddbackend.controllers;

import com.crkomi.uddbackend.Model.LoginJson;
import com.crkomi.uddbackend.Model.PasswordJson;
import com.crkomi.uddbackend.entities.CategoryEntity;
import com.crkomi.uddbackend.entities.UserEntity;
import com.crkomi.uddbackend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<UserEntity> postLoginJson(@RequestBody UserEntity userEntity) {
        if (userEntity == null) {
            return new ResponseEntity<UserEntity>(HttpStatus.BAD_REQUEST);
        }
        if(userService.processingLogging(userEntity) != null){
            return new ResponseEntity<UserEntity>(userService.processingLogging(userEntity), HttpStatus.OK);
        }else{
            return new ResponseEntity<UserEntity>(HttpStatus.UNAUTHORIZED);
        }

    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<UserEntity> update(@PathVariable("id") Long id, @RequestBody UserEntity userEntity){
        UserEntity existingUserEntity= userService.findOne(id);
        if(existingUserEntity == null){
            return new ResponseEntity<UserEntity>(HttpStatus.NOT_FOUND);
        }
        if(userEntity == null){
            return new ResponseEntity<UserEntity>(HttpStatus.BAD_REQUEST);
        }
        if(userService.checkUsername(userEntity)){
            return new ResponseEntity<UserEntity>(userService.save(userEntity), HttpStatus.OK);
        }else{
            return new ResponseEntity<UserEntity>(HttpStatus.BAD_REQUEST);
        }

    }
/*
    @CrossOrigin(origins="*")
    @RequestMapping(value = "/checkusername", method = RequestMethod.POST)
    public ResponseEntity<LoginJson> postCheckUsername(@RequestBody LoginJson loginJson) {
        if (loginJson == null) {
            return new ResponseEntity<LoginJson>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<LoginJson>(userService.checkUsername(loginJson), HttpStatus.OK);
    }
*/
    @CrossOrigin(origins="*")
    @RequestMapping(value = "/changepassword", method = RequestMethod.POST)
    public ResponseEntity<PasswordJson> postChangePassword(@RequestBody PasswordJson passwordJson) {
        if (passwordJson == null) {
            return new ResponseEntity<PasswordJson>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<PasswordJson>(userService.changePassword(passwordJson), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<UserEntity>> getAll(){
        return new ResponseEntity<List<UserEntity>>(userService.findAll(), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<UserEntity> getOne(@PathVariable("id") Long id){
        return new ResponseEntity<UserEntity>(userService.findOne(id), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<UserEntity> create(@RequestBody UserEntity userEntity){
        System.out.println("Pogodjen post usera");
        if(userEntity == null){
            return new ResponseEntity<UserEntity>(HttpStatus.BAD_REQUEST);
        }
        if(userService.checkUsername(userEntity)){
            return new ResponseEntity<UserEntity>(userService.save(userEntity), HttpStatus.OK);
        }else{
            return new ResponseEntity<UserEntity>(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<UserEntity> delete(@PathVariable("id") Long id){
        UserEntity userEntity= userService.findOne(id);
        if(userEntity == null){
            return new ResponseEntity<UserEntity>(HttpStatus.NOT_FOUND);
        }
        userService.delete(id);
        return new ResponseEntity<UserEntity>(userEntity, HttpStatus.OK);
    }



}
