package com.crkomi.uddbackend.lucine.search;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.crkomi.uddbackend.entities.EBookEntity;
import com.crkomi.uddbackend.lucine.indexing.handlers.DocumentHandler;
import com.crkomi.uddbackend.lucine.indexing.handlers.PDFHandler;
import com.crkomi.uddbackend.lucine.model.IndexUnit;
import com.crkomi.uddbackend.lucine.model.RequiredHighlight;
import com.crkomi.uddbackend.services.EBookService;
import org.apache.lucene.search.highlight.Fragmenter;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.highlight.HighlightBuilder;
import org.elasticsearch.search.highlight.HighlightField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

import static org.apache.commons.lang.RandomStringUtils.randomNumeric;

@Service
public class ResultRetriever {

    private ElasticsearchTemplate template;

    @Autowired
    private EBookService eBookService;

    @Autowired
    public ResultRetriever(ElasticsearchTemplate template){
        this.template = template;
    }

    public List<EBookEntity> getResults(org.elasticsearch.index.query.QueryBuilder query,
                                        List<RequiredHighlight> requiredHighlights) {
        if (query == null) {
            return null;
        }

        List<EBookEntity> results = new ArrayList<EBookEntity>();

        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(query)
                .build();

        List<IndexUnit> indexUnits = template.queryForList(searchQuery, IndexUnit.class);

        for (IndexUnit indexUnit : indexUnits) {
            try{
                if(eBookService.findOne(indexUnit.getId()) != null){
                    shouldReturnHighlightedFieldsForGivenQueryAndFields(query, indexUnit.getId(), requiredHighlights);
                }
            } catch (Exception e){
                System.err.println("ne zahteva highlighter");
                e.printStackTrace();
            }
            if(eBookService.findOne(indexUnit.getId()) != null){
                results.add(eBookService.findOne(indexUnit.getId()));
            }
        }


        return results;
    }

    protected DocumentHandler getHandler(String fileName){
        if(fileName.endsWith(".pdf")){
            return new PDFHandler();
        }
        return null;
    }

    public void shouldReturnHighlightedFieldsForGivenQueryAndFields(org.elasticsearch.index.query.QueryBuilder query, Long id, List<RequiredHighlight> requiredHighlights) {

        final List<HighlightBuilder.Field> message = new ArrayList<HighlightBuilder.Field>();
        for(RequiredHighlight requiredHighlight: requiredHighlights){
            message.add(new HighlightBuilder.Field(requiredHighlight.getFieldName()));
        }
        //message.add(new HighlightBuilder.Field("text"));
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(query)
                .withHighlightFields(message.toArray(new HighlightBuilder.Field[message.size()]))
                .build();

        SearchResultMapper searchResultMapper = new SearchResultMapper() {
            public <T> AggregatedPage<T> mapResults(SearchResponse response, Class<T> clazz, Pageable pageable) {
                List<IndexUnit> chunk = new ArrayList<IndexUnit>();
                for (SearchHit searchHit : response.getHits()) {
                    String [] idStrings = searchHit.getId().split("static");
                    String [] bookFilename = eBookService.findOne(id).getFileName().split("static");
                    System.out.println(idStrings[idStrings.length-1]);
                    System.out.println(bookFilename[bookFilename.length-1]);
                    if(!idStrings[idStrings.length-1].equals(bookFilename[bookFilename.length-1])){
                        continue;
                    }
                    if (response.getHits().getHits().length <= 0) {
                        return null;
                    }
                    IndexUnit indexUnit = new IndexUnit();
                    String highlight = "";
                    for (String keySet : searchHit.getHighlightFields().keySet()) {
                        for(int j=searchHit.getHighlightFields().get(keySet).fragments().length - 1; j>= 0 ; j--){
                            highlight += searchHit.getHighlightFields().get(keySet).fragments()[j];
                        }
                    }
                    EBookEntity eBook = eBookService.findOne(id);
                    highlight = highlight.replace("<em>", "<b>");
                    highlight = highlight.replace("</em>", "</b>");
                    eBook.setHighlight(highlight);

                    chunk.add(indexUnit);

                    if (chunk.size() > 0) {
                        return new AggregatedPageImpl<T>((List<T>) chunk);
                    }
                    return null;
                }
                return  null;
            }
        };
        Page<IndexUnit> sampleEntities = template.queryForPage(searchQuery, IndexUnit.class, searchResultMapper);
    }
}
