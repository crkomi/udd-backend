package com.crkomi.uddbackend.lucine.model;

public enum SearchType {

    regular,
    fuzzy,
    phrase,
    range,
    prefix

}
