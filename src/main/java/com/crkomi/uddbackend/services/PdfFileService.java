package com.crkomi.uddbackend.services;

import com.crkomi.uddbackend.controllers.InitController;
import com.crkomi.uddbackend.entities.EBookEntity;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessRead;
import org.apache.pdfbox.io.ScratchFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.common.PDMetadata;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class PdfFileService {

    @Value("${dataDir}")
    private String DATA_DIR_PATH;

    public PdfFileService(){

    }

    public void getMetaData(String pdfPath, EBookEntity eBookEntity){
        System.out.println("<<<<<<<<<<<<<<<<<-------------->>>>>>>>>>>>>>>>>>>");
        try{
            File file = new File(pdfPath);
            PDDocument document = PDDocument.load(file);
            //Getting the PDDocumentInformation object
            PDDocumentInformation pdd = document.getDocumentInformation();

            //Retrieving the info of a PDF document
            System.out.println("Author of the document is :"+ pdd.getAuthor());
            eBookEntity.setAuthor(pdd.getAuthor());
            System.out.println("Title of the document is :"+ pdd.getTitle());
            eBookEntity.setTitle(pdd.getTitle());
            System.out.println("Subject of the document is :"+ pdd.getSubject());

            System.out.println("Creator of the document is :"+ pdd.getCreator());
            System.out.println("Creation date of the document is :"+ pdd.getCreationDate().getTime());
            System.out.println("Modification date of the document is :"+
                    pdd.getModificationDate().getTime());
            System.out.println("Keywords of the document are :"+ pdd.getKeywords());
            eBookEntity.setKeywords(pdd.getKeywords());
            eBookEntity.setMime("application/pdf");
            //Closing the document
            document.close();

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        System.out.println("<<<<<<<<<<<<<<<<<-------------->>>>>>>>>>>>>>>>>>>");
    }

    public EBookEntity saveUploadedFile(MultipartFile file){
        String fileName = null;
        try{
            if (! file.isEmpty()) {
                byte[] bytes = file.getBytes();
                System.out.println("Test" + bytes.length);
                Path path = Paths.get(InitController.abspluteResourceFile + File.separator + file.getOriginalFilename());
                System.out.println("Path is : " + path);
                Files.write(path, bytes);
                fileName = path.toString();
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        System.out.println("Uspesno uplodovan file sa imenom: " + file.getOriginalFilename());
        EBookEntity eBookEntity = new EBookEntity();
        getMetaData(fileName, eBookEntity);
        eBookEntity.setFileName(fileName);
        return eBookEntity;
    }

    private File getResourceFilePath(String path) {
        URL url = PdfFileService.class.getResource(path);
        System.out.println("URL is: " + url);
        File file = null;
        try {
            file = new File(url.toURI());
        } catch (URISyntaxException e) {
            file = new File(url.getPath());
        }
        System.out.println("Ret val is: " + file);
        return file;
    }

}
