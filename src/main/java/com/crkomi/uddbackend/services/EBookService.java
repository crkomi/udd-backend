package com.crkomi.uddbackend.services;

import com.crkomi.uddbackend.entities.CategoryEntity;
import com.crkomi.uddbackend.entities.EBookEntity;
import com.crkomi.uddbackend.lucine.indexing.Indexer;
import com.crkomi.uddbackend.lucine.model.IndexUnit;
import com.crkomi.uddbackend.repositories.CategoryRepository;
import com.crkomi.uddbackend.repositories.EBookRepository;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Paths;
import java.util.List;

@Service
public class EBookService {

    @Autowired
    private EBookRepository eBookRepository;

    @Autowired
    private Indexer indexer;

    public EBookEntity findOne(Long id){
        return eBookRepository.findOne(id);
    }

    public List<EBookEntity> findAll(){
        return eBookRepository.findAll();
    }

    public EBookEntity save(EBookEntity eBookEntity){

        if(eBookEntity.getFileName() != null){

            IndexUnit indexUnit = indexer.getHandler(eBookEntity.getFileName()).getIndexUnit(new File(eBookEntity.getFileName()));
            indexUnit.setTitle(eBookEntity.getTitle());
            indexUnit.setKeywords(eBookEntity.getKeywords());
            indexUnit.setAuthor(eBookEntity.getAuthor());
            indexUnit.setLanguage(eBookEntity.getLanguageEntity().getName());
            indexUnit.convertToLatinic();
            //eBookEntity.setText(indexUnit.getText());
            eBookEntity = eBookRepository.save(eBookEntity);
            indexUnit.setId(eBookEntity.getId());
            indexer.add(indexUnit);
        }
        return eBookEntity;
    }



    public void delete(Long id){
        EBookEntity eBookEntity = findOne(id);
        indexer.delete(eBookEntity.getFileName());
        eBookRepository.delete(id);
    }

}
