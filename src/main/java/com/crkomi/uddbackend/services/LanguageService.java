package com.crkomi.uddbackend.services;

import com.crkomi.uddbackend.entities.EBookEntity;
import com.crkomi.uddbackend.entities.LanguageEntity;
import com.crkomi.uddbackend.repositories.EBookRepository;
import com.crkomi.uddbackend.repositories.LanguageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LanguageService {

    @Autowired
    private LanguageRepository languageRepository;

    public LanguageEntity findOne(Long id){
        return languageRepository.findOne(id);
    }

    public List<LanguageEntity> findAll(){
        return languageRepository.findAll();
    }

    public LanguageEntity save(LanguageEntity languageEntity){
        return languageRepository.save(languageEntity);
    }

    public void delete(Long id){
        languageRepository.delete(id);
    }

}
