package com.crkomi.uddbackend.services;

import com.crkomi.uddbackend.Model.LoginJson;
import com.crkomi.uddbackend.Model.PasswordJson;
import com.crkomi.uddbackend.entities.UserEntity;
import com.crkomi.uddbackend.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    public static final String ADMIN = "Admin";
    public static final String SUBSCRIBER = "Subscriber";

    @Autowired
    private UserRepository userRepository;

    public UserEntity findOne(Long id){
        return userRepository.findOne(id);
    }

    public List<UserEntity> findAll(){
        return userRepository.findAll();
    }

    public UserEntity save(UserEntity userEntity){
        return userRepository.save(userEntity);
    }

    public void delete(Long id){
        userRepository.delete(id);
    }

    public UserEntity processingLogging(UserEntity userEntity){
        List<UserEntity> userEntities = userRepository.findByUsernameAndPassword(userEntity.getUsername(), userEntity.getPassword());
        if(userEntities.size() > 0){
            return userEntities.get(0);
        }else {
            return null;
        }
    }

    public LoginJson processingUpdate(LoginJson loginJson, Long id){
        UserEntity userEntity = findOne(id);
        userEntity.setUsername(loginJson.getUsername());
        userEntity.setPassword(loginJson.getPassword());
        userEntity.setLastname(loginJson.getLastname());
        userEntity.setFirstname(loginJson.getFirstname());
        loginJson.setOldUsername(loginJson.getUsername());
        userEntity = save(userEntity);
        return loginJson;
    }

    public boolean checkUsername(UserEntity userEntity){
        List<UserEntity> userEntityList = userRepository.findByUsername(userEntity.getUsername());
        UserEntity userEntityOriginal = userRepository.findOne(userEntity.getId());
        if(userEntityList.size() > 0){
            if(userEntityOriginal.getUsername().equals(userEntity.getUsername())){
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }

    public PasswordJson changePassword(PasswordJson passwordJson){
        UserEntity userEntity = findOne(passwordJson.getUserId());
        if(userEntity != null){
            if(userEntity.getPassword().equals(passwordJson.getOldPassword())){
                if(passwordJson.getNewPassword().equals(passwordJson.getRepeatNewPassword())){
                    passwordJson.setStatus(true);
                    userEntity.setPassword(passwordJson.getNewPassword());
                    userEntity = userRepository.save(userEntity);
                    return passwordJson;
                }
            }
        }
        passwordJson.setStatus(false);
        return passwordJson;
    }

}
