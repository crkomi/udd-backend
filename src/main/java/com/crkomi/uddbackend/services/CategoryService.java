package com.crkomi.uddbackend.services;

import com.crkomi.uddbackend.entities.CategoryEntity;
import com.crkomi.uddbackend.entities.EBookEntity;
import com.crkomi.uddbackend.entities.UserEntity;
import com.crkomi.uddbackend.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private EBookService eBookService;

    public CategoryEntity findOne(Long id){
        return categoryRepository.findOne(id);
    }

    public List<CategoryEntity> findAll(){
        return categoryRepository.findAll();
    }

    public CategoryEntity save(CategoryEntity categoryEntity){
        return categoryRepository.save(categoryEntity);
    }

    public void delete(Long id){
        CategoryEntity categoryEntity = findOne(id);
        for(EBookEntity eBookEntity : categoryEntity.geteBookEntityList()){
            eBookService.delete(eBookEntity.getId());
        }
        categoryRepository.delete(id);
    }

    public boolean checkName(CategoryEntity categoryEntity){
        List<CategoryEntity> userCategoryEntity = categoryRepository.findByName(categoryEntity.getName());
        CategoryEntity categoryEntityOriginal = categoryRepository.findOne(categoryEntity.getId());
        if(userCategoryEntity.size() > 0){
            if(categoryEntityOriginal.getName().equals(categoryEntity.getName())){
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }

}
